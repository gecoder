
$: << ".."

require '../ext/lib/gecode'

have_explorer = false

begin
	have_explorer = require 'explorer/explorer'
ensure
end

n = 9

fd = Gecode::FD.new([ 's', 'e', 'n', 'd', 'm', 'o', 'r', 'y'], (0..n))

fd.rel fd.s, '!=', 0
fd.rel fd.m, '!=', 0


fd.post(              (fd.s * 1000 + fd.e * 100 + fd.n * 10  + fd.d   + 
                       fd.m * 1000 + fd.o * 100 + fd.r * 10  + fd.e).
equal( fd.m * 10000 +  fd.o * 1000 + fd.n * 100 + fd.e * 10  + fd.y ) )

array = fd.space.solution
Gecode::dom(fd.space, array, 0, 10, Gecode::ICL_DEF)

fd.distinct
fd.distribute

unless have_explorer
	stats = fd.search(1)
	
	puts fd.default
	puts "Time: #{stats.time} seconds"
else
	Gecode::Explorer::search(fd.space, 1)
end

