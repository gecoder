
require '../ext/lib/gecode'

n = (ARGV[0] || 8).to_i
fd = Gecode::FD.new(n, (1..n))

fd.distinct((0...n).to_a)
fd.distinct(((1-n)..0).to_a.reverse)
fd.distinct
fd.distribute

stats = fd.search(1)


