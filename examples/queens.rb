
require '../ext/lib/gecode'

$: << ".."

have_explorer = false
begin
	have_explorer = require 'explorer/explorer'
ensure
end


n = 10

fd = Gecode::FD.new(n, (1..n))

naive = false

if naive
	for i in (0..(fd.size-1))
		for j in (i+1..(fd.size-1))
			fd.post(fd[i].different( fd[j]) )
			fd.post((fd[i]+i).different(fd[j]+j) )
			fd.post((fd[i]-i).different(fd[j]-j) )
		end
	end
else
	fd.distinct((0...n).to_a)
	fd.distinct(((1-n)..0).to_a.reverse)
	
	fd.distinct
end


fd.distribute

unless have_explorer
	stats = fd.search(1)
	
	puts "Time: #{stats.time} seconds"
	puts "Propagations: #{stats.propagate}"
	puts "Memory: #{(stats.memory+1023) / 1024}kb"
	
	fd.default.debug
else
	class Inspector
		include Gecode::Explorer::NodeInspector
		
		def inspectSpace(space)
			return if space.nil?
			
			if space.status == Gecode::SS_SOLVED
				puts "========================"
				array = space.solution
				
				puts array
				
				n = array.size
				
				n.times { |i|
					n.times { |j|
						if (j+1) == array.at(i).max
							print " Q "
						else
							print " - "
						end
					}
					print "\n"
				}
			end
		end
		
		def preinspectSpace(space)
			inspectSpace(space)
		end
	end
	
	Gecode::Explorer::search(fd.space, 1, Inspector.new)
end


