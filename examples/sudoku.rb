
require '../ext/lib/gecode'

n = 3

space = Gecode::Space.new

sva = Gecode::IntVarArray.new(space, n*n*n*n, 1, n*n)
space.own(sva, "solution")

nn = n*n

nn.times { |i|
	row = Gecode::IntVarArray.new(space, nn,1,n*n)
	col = Gecode::IntVarArray.new(space, nn,1,n*n)
	
	nn.times { |j|
		row[j] = sva[j+i*nn]
		col[j] = sva[i+(j*nn)]
	}
	
	Gecode::distinct(space, row, Gecode::ICL_DEF)
	Gecode::distinct(space, col, Gecode::ICL_DEF)
}


nn.times { |block|
	blocks = Gecode::IntVarArray.new(space, nn)
	
	row = block / n
	
	n.times { |i|
		n.times { |j|
			blocks[j+(i*n)] = sva[j+(i*nn) + (block*n) + (row*nn*2)]
		}
	}
	
	Gecode::distinct(space, blocks, Gecode::ICL_DEF)
}

Gecode::branch(space, sva, Gecode::BVAR_SIZE_MIN, Gecode::BVAL_SPLIT_MIN);

stop = Gecode::Search::Stop.new
dfs = Gecode::DFS.new(space, 8, 2, stop)

puts "Searching..."

solved = dfs.next


nn.times { |i|
	nn.times { |j|
		print solved.solution[j+i*nn]
	}
	
	puts ""
}



