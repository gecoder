
require '../gecode'

n = 3
nn = n*n

space = Gecode::Space.new

sva = Gecode::SetVarArray.new(space, n*n, Gecode::IntSet::EMPTY, 1, n*n*n*n, 9,9)
space.own(sva, "solution")

row = Array.new(9)
col = Array.new(9)
block = Array.new(9)

9.times { |i|
	dsr = Gecode::IntSet.new((i*nn)+1, (i*nn)+9)
	row[i] = dsr
	
	dsc_arr = [ 1+i, 10+i, 19+i, 28+i, 37+i, 46+i, 55+i, 64+i, 73+i ]
	
	dsc = Gecode::IntSet.new(dsc_arr, 9)
	
	col[i] = dsc
}

3.times { |i|
	3.times { |j|
		dsb_arr = [ (j*27)+(i*3)+1, (j*27)+(i*3)+2, (j*27)+(i*3)+3,(j*27)+(i*3)+10, (j*27)+(i*3)+11, (j*27)+(i*3)+12, (j*27)+(i*3)+19, (j*27)+(i*3)+20, (j*27)+(i*3)+21 ]
		
		dsb = Gecode::IntSet.new(dsb_arr, 9)
		block[i*3+j] = dsb
		
	}
}

(nn-1).times { |i|
	(i+1).upto(nn-1) { |j|
		Gecode::rel(space, space.solution.at(i), Gecode::SRT_DISJ, space.solution.at(j))
	}
}

puts "DIST"

Gecode::distinct(space, space.solution, nn)

nn.times { |i|
	nn.times { |j|
		inter_row = Gecode::SetVar.new(space, Gecode::IntSet::EMPTY, 1, 9*9, 1, 1)
		
		Gecode::rel(space, space.solution.at(i), Gecode::SOT_INTER, row[j], Gecode::SRT_EQ, inter_row)
		
		inter_col = Gecode::SetVar.new(space, Gecode::IntSet::EMPTY, 1, 9*9, 1, 1)
		Gecode::rel(space, space.solution.at(i), Gecode::SOT_INTER, col[j], Gecode::SRT_EQ, inter_col)
		
		inter_block = Gecode::SetVar.new(space, Gecode::IntSet::EMPTY, 1, 9*9, 1, 1)
		Gecode::rel(space, space.solution.at(i), Gecode::SOT_INTER, block[j], Gecode::SRT_EQ, inter_block)
	}
}

puts "Branch"
Gecode::branch(space, space.solution, Gecode::SETBVAR_NONE, Gecode::SETBVAL_MIN)


def printSolution(solution, n)
	puts "###########"
	(n*n*n*n).times { |i|
		(n*n).times { |j|
			if solution.at(j).contains(i+1)
				if j+1 < 10
					print "#{j+1} "
				else
					print "#{(j.to_i+1+'A'.to_i-10).chr} "
				end
				break
			end
		}
		
		if (i+1) % (n*n) == 0
			puts ""
		end
	}
	puts "###########"
end


puts "SEARCHING"
stop = Gecode::Search::Stop.new
dfs = Gecode::DFS.new(space, 8, 2, stop)

while not space.nil?
	break if space.nil?
	space.solution.debug
	space = dfs.next
	
	printSolution(space.solution, n)
end

puts "END"
