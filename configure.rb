
require 'mkmf'

rust_dir = Dir.getwd+"/rust"

$: << rust_dir

find_library("gecodeint", "" )
find_library("gecodekernel", "")
find_library("gecodeminimodel", "")
find_library("gecodesearch", "")
find_library("gecodeset", "")


cppflags = "-I#{rust_dir}/include"

with_cppflags(cppflags) {
	find_header("rust_conversions.hh", "./rust/include" )
	find_header("rust_checks.hh", "./rust/include" )
}

Dir.chdir("ext")
load "rgecode.rb"
Dir.chdir("..")

#$extout = "ext"
create_makefile("Gecode", "ext")


