# Copyright (c) 2007, David Cuadrado <krawek@gmail.com>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
# 3. Documentation and/or other materials provided with the distribution. 
#    All advertising materials mentioning features or use of this software  
#    must display the following acknowledgement: 
#    This product includes software developed by David Cuadrado and contributors.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

require 'explorer/node'

require 'gecode'

app = Qt::Application.new(ARGV)


qgv = Qt::GraphicsView.new
qgv.setRenderHints( Qt::Painter::Antialiasing)

scene = Qt::GraphicsScene.new

root = Gecode::Explorer::SpaceNode.new(nil, "root")
root.state = 0

scene.addItem root

node1 = Gecode::Explorer::SpaceNode.new(root, "node1")
scene.addItem node1
node1.state = 0

node2 = Gecode::Explorer::SpaceNode.new(root, "node2")
scene.addItem node2

node3 = Gecode::Explorer::SpaceNode.new(node2, "node3")
scene.addItem node3

node4 = Gecode::Explorer::SpaceNode.new(node3, "node4")
scene.addItem node4

node5 = Gecode::Explorer::SpaceNode.new(node3, "node5")
scene.addItem node5

node6 = Gecode::Explorer::SpaceNode.new(node3, "node6")
scene.addItem node6

qgv.setScene(scene)

qgv.show



app.exec

