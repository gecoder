# Copyright (c) 2007, David Cuadrado <krawek@gmail.com>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
# 3. Documentation and/or other materials provided with the distribution. 
#    All advertising materials mentioning features or use of this software  
#    must display the following acknowledgement: 
#    This product includes software developed by David Cuadrado and contributors.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

require 'Qt4'
require 'explorer/treelayout'

module Gecode

module Explorer

module NodeInspector # mixin
	def inspectSpace(space)
	end
	
	def preinspectSpace(space)
	end
end

class Node < Qt::GraphicsItem
	attr_accessor :childrens
	attr_accessor :parent
	attr_accessor :alternative
	
	attr_reader :knownChildCount
	attr_accessor :dirty
	
	def initialize()
		super()
		
		@childrens = []
		@knownChildCount = -1
		@alternative = 0
		
		@dirty = true
	end
	
	def root?
		@parent.nil?
	end
	
	def depth
		root? ? 1 : @parent.depth + 1
	end
	
	def paint(painter, option, widget)
		painter.drawRect boundingRect
	end
	
	def boundingRect
		r = Qt::RectF.new(0, 0, 14, 14)
		
# 		child1 = @childrens.first
# 		child2 = nil
# 		
# 		@childrens.reverse_each { |child|
# 			unless child.nil?
# 				child2 = child
# 				break
# 			end
# 		}
		
# 		return r if child1.nil? or child2.nil?
		
# 		return Qt::RectF.new(0, 0, ([child2.x, r.x].max - [child1.x, r.x].min).abs, (r.y - child1.y).abs )
		
		#########################
		
# 		if @knownChildCount > 1
# 			
# 			marginX = 30
# 			marginY = 30
# 			
# 			child1 = @childrens.first
# 			child2 = nil
# 			
# 			@childrens.reverse_each { |lc|
# 				unless lc.nil?
# 					child2 = lc
# 					break
# 				end
# 			}
# 			
# 			rchild1 = child1.boundingRect
# 			rchild2 = child2.boundingRect
# 			
# 			xIni =  (r.center().x() - ((rchild2.width + rchild1.width + marginX) / 2))
# 			xEnd =  xIni + marginX + rchild1.width + rchild2.width
# 			
# 			return Qt::RectF.new(xIni, 0, (xEnd - xIni).abs , [rchild1.height, rchild2.height].max + marginY )
# 		end
# 		
		r
	end
	
	def setNumberOfChildrens(n)
		@knownChildCount = n;
		if n < childrens.size
			while childrens.size > kids
				childrens.delete_at childrens.size
			end
		else
			@childrens[n] = nil
		end
	end
	
	def hasChildrens?
		@knownChildCount > 0
	end
	
	def setChild(index, child)
		child.parent = self
		scene.addItem(child)
		
		@childrens[index] = child
		child.alternative = index
	end
	
	def child(index)
		@childrens[index]
	end
	
	def relayout
		puts "Relayout..."
		layout
		order
		nil
	end
	
	def layout
		cursor = TreeOrder.new(self)
		visitor = PostOrderNodeVisitor.new(cursor)
		visitor.travel
	end
	
	def order
		cursor = TreeLayout.new(self)
		visitor = PreOrderNodeVisitor.new(cursor)
		visitor.travel
	end
	
end

class SpaceNode < Node
	attr_writer :space
	attr_accessor :status
	
	attr_accessor :copy
	attr_reader :desc
	
	attr_reader :curBest
	
	attr_accessor :hasSolvedChildren
	attr_accessor :hasFailedChildren
	
	attr_accessor :xshape
	attr_accessor :xboundingBox
	attr_accessor :offset
	
	ST_SOLVED = 0x0
	ST_FAILED = 0x1
	ST_BRANCH = 0x2
	ST_UNDETERMINED = 0x04
	
	class Config
		A_D = 100000
		MRD = 1
	end
	
	class Branch
		attr_reader :alternative
		attr_reader :desc
		attr_reader :ownBest
		def initialize(alt, desc, best = nil)
			@alternative = alt
			@desc = desc
			@ownBest = best
		end
	end
	
	def initialize(*args)
		super()
		
		@status = ST_UNDETERMINED
		@hasSolvedChildren = false
		@hasFailedChildren = false
		
		@space = nil
		@desc = nil
		@curBest = nil
		
		@openChildrenCount = 0
		@offset = 0
		
		case args.size
			when 1
				if args.first.kind_of?(Fixnum)
					alternative = args.first
				else
					@space = args[0]
					
					if @space.nil?
						@status = ST_FAILED
						@copy = @space
						@hasFailedChildren = true
						
						childrens.clear
					else
						if not @space.failed
							@copy = @space.clone(true)
						else
							@copy = @space
						end
					end
				end
		end
# 		setFlag( Qt::GraphicsItem::ItemIsMovable, true)
		setAcceptsHoverEvents(true)
		setCursor(Qt::Cursor.new(Qt::PointingHandCursor))
	end
	
	def space
		acquireSpace
		
		if @space.status != Gecode::SS_FAILED
			return @space.clone(true)
		end
		
		@space
	end
	
	def paint(painter, option, widget)
		rect = Qt::RectF.new(0, 0, 14, 14)
		
		case @status
			when ST_BRANCH
				painter.setBrush(Qt::Brush.new(Qt::Color.new("#0000ff")))
				painter.drawEllipse rect
			when ST_SOLVED
				triangle = Qt::PainterPath.new()
				
				triangle.moveTo(Qt::PointF.new( rect.width / 2, 0 ))
				
				triangle.lineTo( rect.bottomRight )
				triangle.lineTo( rect.bottomLeft )
				
				painter.setBrush(Qt::Brush.new(Qt::Color.new("#00ff00")))
				painter.drawPath(triangle)
			when ST_FAILED
				painter.setBrush(Qt::Brush.new(Qt::Color.new("#ff0000")))
				painter.drawRect rect
			when ST_UNDETERMINED
				painter.setBrush(Qt::white)
				painter.drawEllipse(rect)
		end
		
		inverted = sceneMatrix().inverted()
		br = sceneMatrix.mapRect(Qt::RectF.new(0,0,14,14))
		
		childrens.each { |child|
			next if child.nil?
			brchild = child.mapToItem(child, Qt::RectF.new(0,0,14,14) ).boundingRect
			painter.drawLine( inverted.map(Qt::PointF.new(br.center().x(), br.bottom())), inverted.map(child.pos)+ Qt::PointF.new(brchild.center.x ,0) )
		}
		
		painter.setBrush( Qt::Brush.new(Qt::transparent) )
	end
	
	def donateSpace(alt)
		ret = @space
		if not ret.nil?
			@space = @space.clone(false)
			ret.commit(@desc, alt)
		end
		
		ret
	end
	
	def solveUp
		@hasSolvedChildren = true
		if not parent.nil? and not parent.hasSolvedChildren
			parent.solveUp
		end
	end
	
	def closeChild(hadFailures, hadSolutions)
		@openChildrenCount -= 1
		@hasFailedChildren = @hasFailedChildren or hadFailures
		@hasSolvedChildren = @hasSolvedChildren or hadSolutions
		
		if @openChildrenCount == 0
			if not parent.nil?
				parent.closeChild(@hasFailedChildren, @hasSolvedChildren)
			end
		elsif hadSolutions
			solveUp()
		end
	end
	
	def open?
		@status == ST_UNDETERMINED || @openChildrenCount > 0
	end
	
	def numberOfChildNodes
		kids = knownChildCount
		
		if kids == -1
			acquireSpace()
			case @space.status
				when SS_FAILED:
					@space = nil
					kids = 0
					
					@hasSolvedChildren = false
					@hasFailedChildren = true
					
					@status = ST_FAILED
					if not root?
						parent.closeChild(true, false)
					end
					
					setCursor(Qt::Cursor.new(Qt::OpenHandCursor))
				when SS_SOLVED:
					kids = 0
					@status = ST_SOLVED
					
					@hasSolvedChildren = true
					@hasFailedChildren = false
					
					if not @curBest.nil?
						@curBest = @space.clone(true)
					end
					
					if not root?
						parent.closeChild(false, true)
					end
				when SS_BRANCH:
					@desc = @space.description()
					kids = @desc.alternatives()
					@status = ST_BRANCH
			end
			
			dirtyUp
			
			@openChildrenCount = kids
			setNumberOfChildrens(kids)
			
			
			(kids-1).downto(0) { |i|
				child = SpaceNode.new(i)
				setChild(i, child)
			}
			
		end
		
		kids
	end
	
	private
	def recompute
		rdist = 0
		
		if @space.nil?
			curNode = self
			
			stack = []
			
			
			while curNode.copy.nil?
				b = Branch.new(curNode.alternative, parent.desc, @curBest.nil? ? nil : curNode.ownBest)
				stack.push(b)
				
				curNode = curNode.parent
				rdist += 1
			end
			
			curSpace = curNode.copy.clone(true)
			middleNode = curNode
			
			curDist = 0
			
			until stack.empty?
				if Config::A_D >= 0 and curDist > Config::A_D and curDist == rdist / 2
					middleNode.copy = curSpace.clone(true)
				end
				
				b = stack.pop()
				curSpace.commit(b.desc, b.alternative)
				
				curDist += 1
				
				middleNode = middleNode.childrens[b.alternative]
			end
			
			@space = curSpace.clone(false)
		end
		
		return rdist
	end
	
	def checkLAO(alt)
		ret = nil
		if not @copy.nil? and @openChildrenCount == 1 and not root?
			#last alternative optimization
			ret = @copy
			@copy = nil
			
			ret.commit(@desc, alt)
		end
		
		ret
	end
	public :checkLAO
	
	def acquireSpace
		if @space.nil? and not root?
			@space = parent.donateSpace(alternative)
			
			@space = @space.clone(false) if @space and @space.status != Gecode::SS_FAILED
		end
		
		if @space.nil?
			if recompute() > Config::MRD and Config::MRD >= 0 and @space.status() == Gecode::SS_BRANCH
				@copy = @space.clone(true)
			end
		end
		
		if @copy.nil? and not parent.nil?
			@copy = parent.checkLAO(alternative)
		end
	end
	
	def dirtyUp
		cur = self
		until cur.dirty
			cur.dirty = true
			unless cur.root?
				cur = cur.parent
			end
		end
		
		order
	end
	public :dirtyUp
	
	protected
	def mousePressEvent(event)
		layout
		super
	end
	
	def mousePressEvent(event)
		case @status
			when ST_FAILED
			when ST_UNDETERMINED
				numberOfChildNodes
				
				dirtyUp
				
				relayout
			else
				scene.inspectSpace(@space)
		end
	end
	
	def hoverEnterEvent(event)
		scene.preinspectSpace(@space)
	end
	
	def hoverLeaveEvent(event)
	end
	
# 	def itemChange(change, value)
# 		case change
# 			when Qt::GraphicsItem::ItemPositionChange
# 				if not root?
# 					update
# 				else
# 					update
# 				end
# 		end
# 		
# 		super
# 	end
	
	def debug(fun)
		puts "In function: #{fun}"
		puts "Space: #{@space}"
		puts "Space status: #{@space.status}" if @space
		puts "Node status: #{@status}"
		puts "Alternative: #{alternative}"
		puts "Depth: #{depth}"
		puts "--------------------"
	end
	public :debug
end

end

end #module

