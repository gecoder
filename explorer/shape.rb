# Copyright (c) 2007, David Cuadrado <krawek@gmail.com>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
# 3. Documentation and/or other materials provided with the distribution. 
#    All advertising materials mentioning features or use of this software  
#    must display the following acknowledgement: 
#    This product includes software developed by David Cuadrado and contributors.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

module Gecode

module Explorer

class BoundingBox
	attr_reader :left, :right, :depth
	def initialize(left, right, depth)
		@left = left
		@right = right
		@depth = depth
	end
end

class Extent
	attr_accessor :l, :r
	
	def initialize(*args)
		case args.size
			when 0
				@l = -1
				@r = -1
			when 1
				halfWidth = args[0] / 2
				@l = 0 - halfWidth
				@r = 0 + halfWidth
			when 2
				@l = args[0]
				@r = args[1]
		end
	end
	
	def extend(deltaL, deltaR)
		@l += deltaL
		@r += deltaR;
	end
	
	def move(delta)
		@l += delta
		@r += delta
	end
end



class Shape
# 	attr_reader :shape
	
	def initialize(*args)
		@shape = []
		case args.size
			when 0
			when 1
				if args[0].kind_of?(Extent)
					@shape << args[0]
				elsif args[0].kind_of?(Shape)
					args[0].depth.times { |i|
						@shape[i] = args[0].get(i)
					}
				else
					raise "Unknown constructor!"
				end
			when 2
				extent = args[0]
				subShape = args[1]
				
				@shape[0] = extent
			
				subShape.depth.times { |i|
					@shape[i+1] = subShape.get(i)
				}
			else
				raise "Unknown constructor!"
		end
	end
	
	def add(e)
		@shape << e
	end
	
	def get(i)
		@shape[i]
	end
	
	def extend(deltaL, deltaR)
		if @shape.size > 0
			@shape.first.extend(deltaL, deltaR)
		end
	end
	
	def move(delta)
		if @shape.size > 0
			@shape.first.move(delta)
		end
	end
	
	def extentAtDepth(depth, extent)
		currentDepth = 0;
		extentL = 0;
		extentR = 0;
		
		@shape.each { |currentExtent|
			break if currentDepth > depth
			
			
			extentL += currentExtent.l;
			extentR += currentExtent.r;
			
			currentDepth += 1
		}
		
		if currentDepth == depth + 1
			extent = Extent.new(extentL, extentR)
			true
		else
			false
		end
	end
	
	def boundingBox
		lastLeft = 0;
		lastRight = 0;
		left = 0;
		right = 0;
		depth = 0;
		
		@shape.each { |curExtent|
			depth+=1
			lastLeft = lastLeft + curExtent.l;
			lastRight = lastRight + curExtent.r;
			if lastLeft < left
				left = lastLeft;
			end
			
			if lastRight > right
				right = lastRight;
			end
		}
		BoundingBox.new(left, right, depth);    
	end
	
	def depth
		@shape.size
	end
	
end

class ShapeList
	attr_reader :shapes
	attr_reader :offsets
	
	def initialize(length, minSep)
		@shapes = Array.new(length)
		@offsets = Array.new(length)
		@minSep = minSep
	end
	
	def shape(i)
		@shapes[i]
	end
	
	def offset(i)
		@offset[i]
	end
	
	def mergedShape
		numberOfShapes = @shapes.size()
		
		
		if numberOfShapes == 1
			@offsets[0] = 0;
			return Shape.new(@shapes[0])
		else
			alphaL = Array.new(numberOfShapes);
			alphaR = Array.new(numberOfShapes);
			
			width = 0;
			
			currentShapeL = Shape.new(@shapes[0])
			currentShapeR = Shape.new(@shapes[numberOfShapes - 1])
			
			1.upto( numberOfShapes-1 ) { |i|
				
				nextShapeL = @shapes[i]
				
				nextAlphaL = alpha(currentShapeL, nextShapeL)
				
				currentShapeL = self.class.merge(currentShapeL, nextShapeL, nextAlphaL)
				
				alphaL[i] = nextAlphaL - width
				
				width = nextAlphaL;
					
				nextShapeR = @shapes[numberOfShapes - 1 - i]
				nextAlphaR = alpha(nextShapeR, currentShapeR)
				
				currentShapeR = self.class.merge(nextShapeR, currentShapeR, nextAlphaR)
				
				alphaR[numberOfShapes - i] = nextAlphaR
			}
			
			mergedShape = currentShapeR;
			halfWidth = width / 2
			mergedShape.move(- halfWidth);
				
			offset = - halfWidth;
			@offsets[0] = offset
			
			1.upto( numberOfShapes-1 ) { |i|
				offset += (alphaL[i] + alphaR[i]) / 2;
				@offsets[i] = offset;
			}
			
			mergedShape;
		end
	end
	
	private
	def alpha(shape1, shape2)
		alpha = @minSep
		
		extentR = 0
		extentL = 0
		
		depth1 = shape1.depth
		depth2 = shape2.depth
		
		to = [depth1, depth2].min
		to.times { |i|
			extentR += shape1.get(i).r;
			extentL += shape2.get(i).l;
			alpha = [alpha, extentR - extentL + @minSep].max
		}
		alpha
	end
	
	def self.merge(shape1, shape2, alpha)
		if shape1.depth() == 0
			return Shape.new(shape2)
		elsif shape2.depth() == 0
			return Shape.new(shape1)
		else
			result = Shape.new();
			
			topmostL = shape1.get(0).l
			topmostR = shape2.get(0).r
			
			topmostExtent = Extent.new(topmostL, topmostR)
			topmostExtent.extend(0, alpha)
			
			result.add(topmostExtent);
			
			backoffTo1 = shape1.get(0).r - alpha - shape2.get(0).r;
			backoffTo2 = shape2.get(0).l + alpha - shape1.get(0).l;
			
			min = [shape1.depth(), shape2.depth()].min
			
			min.times { |i|
				currentExtent1 = shape1.get(i);
				currentExtent2 = shape2.get(i);
				
				newExtentL = currentExtent1.l;
				newExtentR = currentExtent2.r;
				
				newExtent = Extent.new(newExtentL, newExtentR);
				
				result.add(newExtent);
				
				backoffTo1 += currentExtent1.r - currentExtent2.r;
				backoffTo2 += currentExtent2.l - currentExtent1.l;
			}
			
			if min < shape1.depth
				currentExtent1 = shape1.get(min);
				min += 1
				
				newExtentL = currentExtent1.l;
				newExtentR = currentExtent1.r;
				
				newExtent = Extent.new(newExtentL, newExtentR);
				newExtent.extend(0, backoffTo1);
				
				result.add(newExtent);
				
				[min, shape1.depth()].min.times { |i|
					result.add(shape1.get(i));
					min += 1
				}
			end
			
			if min < shape2.depth
				currentExtent2 = shape2.get(min);
				min += 1
				newExtentL = currentExtent2.l;
				newExtentR = currentExtent2.r;
				
				newExtent = Extent.new(newExtentL, newExtentR);
				newExtent.extend(backoffTo2, 0);
				result.add(newExtent);
				
				[min, shape2.depth()].min.times { |i|
					result.add(shape2.get(i));
				}
			end
			
			result;
		end
	end
end


end # Explorer
end # Gecode