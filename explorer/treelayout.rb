# Copyright (c) 2007, David Cuadrado <krawek@gmail.com>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
# 3. Documentation and/or other materials provided with the distribution. 
#    All advertising materials mentioning features or use of this software  
#    must display the following acknowledgement: 
#    This product includes software developed by David Cuadrado and contributors.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

require 'Qt'

require 'explorer/shape'

module Gecode

module Explorer

class Cursor
	attr_reader :current
	attr_reader :root
	
	def initialize(root)
		@root = root
		@current = root
	end
	
	def canMoveDown?
		@current.knownChildCount > 0
	end
	
	def moveDown
		@current = @current.child(0)
	end
	
	def canMoveSide?
		not @current.root? and @current.alternative < @current.parent.knownChildCount - 1
	end
	
	def moveSide
		@current = @current.parent.child(@current.alternative+1);
	end	
	
	
	def canMoveUp?
		@current != @root and not @current.root?
	end
	
	def moveUp
		@current = @current.parent
	end
	
	def process
		raise NotImplementedError
	end
end


class NodeVisitor
	attr_reader :cursor
	def initialize(cursor)
		@cursor = cursor
	end
	
	def nextNode
		raise NotImplementedError
	end
	
	def travel
		while nextNode
		end
	end
end

class PreOrderNodeVisitor < NodeVisitor
	def initialize(cursor)
		super
	end
	
	def nextNode
		cursor.process
		if cursor.canMoveDown?
			cursor.moveDown
		elsif cursor.canMoveSide?
			cursor.moveSide
		else
			return backtrack()
		end
		true
	end
	
	def backtrack
		while not cursor.canMoveSide? and cursor.canMoveUp?
			cursor.moveUp
		end
		
		if not cursor.canMoveUp?
			return false
		end
		
		cursor.moveSide
		true;
	end
end

class PostOrderNodeVisitor < NodeVisitor
	def initialize(cursor)
		super
		moveToLeaf
	end
	
	def nextNode
		cursor.process
		
		if cursor.canMoveSide?
			cursor.moveSide
			moveToLeaf()
		elsif cursor.canMoveUp?
			cursor.moveUp();
		else
			return false
		end
		true
	end
	
	def moveToLeaf
		while cursor.canMoveDown?
			cursor.moveDown
    	end
	end
end

class TreeOrder < Cursor
	def initialize(root)
		super
	end
	
	def canMoveDown?
		ok = super 
		ok and current.dirty
	end
	
	def process
		if current.dirty
			extent = Extent.new(20)
			numberOfChildren = current.knownChildCount
			
			shape = nil
			if numberOfChildren == -1
				shape = Shape.new(extent)
# 			elsif current.isHidden()
# 				shape = Shape.new(extent)
			elsif numberOfChildren == 0
				shape = Shape.new(extent)
			else
				childShapes = ShapeList.new(numberOfChildren, 10)
				
				numberOfChildren.times { |i|
					childShapes.shapes[i] = current.child(i).xshape()
				}
				
				subtreeShape = childShapes.mergedShape()
				subtreeShape.extend(- extent.l, - extent.r)
				shape = Shape.new(extent, subtreeShape)
				
				numberOfChildren.times { |i|
					current.child(i).offset = childShapes.offsets[i]
				}
			end
			
			current.xshape = shape
			current.xboundingBox = shape.boundingBox
			current.dirty = false
		end
	end
	
end

class TreeLayout < Cursor
	def initialize(root)
		super
		
		@x = 0
		@y = 0
	end
	
	def process
		parentX = @x - current.offset()
		parentY = @y + 20;
	
		myx = @x;
		myy = 38 + @y
		
		current.setPos(myx-10, myy)
		
		current.update
	end
	
	def canMoveDown?
		super
	end
	
	def moveDown
		super
		
		@x += current.offset();
		@y += 38;
	end
	
	def moveSide
		@x -= current.offset();
		super
		@x += current.offset();
	end	
	
	def moveUp
		@x -= current.offset
		@y -= 38
		super
	end
	
# 	def clipped?
# 		BoundingBox b = current.xboundingBox
# 		(@x + b.left > @clippingRect.x() + @clippingRect.width() or @x + b.right < @clippingRect.x() or @y > @clippingRect.y() + @clippingRect.height() or @y + b.depth * 38 < @clippingRect.y())
# 	end
end


end # Explorer

end # Gecode



