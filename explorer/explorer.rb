# Copyright (c) 2007, David Cuadrado <krawek@gmail.com>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
# 3. Documentation and/or other materials provided with the distribution. 
#    All advertising materials mentioning features or use of this software  
#    must display the following acknowledgement: 
#    This product includes software developed by David Cuadrado and contributors.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

require 'Qt4'
require 'gecode'
require 'explorer/node'

module Gecode

module Explorer

class Scene < Qt::GraphicsScene
	attr_accessor :inspector
	include NodeInspector
	
	def inspectSpace(space)
		inspector.inspectSpace(space) unless @inspector.nil?
		
	end
	
	def preinspectSpace(space)
		inspector.preinspectSpace(space) unless @inspector.nil?
	end
end

class TreeCanvas < Qt::GraphicsView
	def initialize(space, inspector = nil)
		super()
		
		@rootSpace = nil
		
		if space.status != Gecode::SS_FAILED
			@rootSpace = space.clone(true)
		end
		
		setRenderHints( Qt::Painter::Antialiasing)
		
		@scene = Scene.new
		@scene.inspector = inspector
		
		@root = SpaceNode.new(@rootSpace)
		@scene.addItem(@root)
		
		setScene(@scene)
		
		setDragMode(Qt::GraphicsView::ScrollHandDrag)

		setBackgroundBrush(Qt::Brush.new( Qt::Pixmap.new(File.dirname(__FILE__)+"/tile.png")) )
	end
	
	def search(n = 0)
		stack = [ @root ]
		
		solutions = 0
		while not stack.empty?
			node = stack.pop()
			if node.open?
				kids = node.numberOfChildNodes()
				if node.status() == SpaceNode::ST_SOLVED and n > 0
					solutions += 1
					break if solutions >= n
				end
				
				(kids - 1).downto(0) { |i|
					stack.push node.childrens[i]
				}
			end
		end
		
		@root.relayout
	end
	
	def mousePressEvent(event)
		super
		
		relayoutTree
	end
	
	def relayoutTree
		@root.layout
		
		@root.relayout
		viewport.repaint
	end
	
	def scaleView(scaleFactor)
		factor = matrix().scale(scaleFactor, scaleFactor).mapRect(Qt::RectF.new(0, 0, 1, 1)).width();
		unless factor < 0.07 or factor > 100
			scale(scaleFactor, scaleFactor)
		end
	end
	
	def wheelEvent(event)
		if event.modifiers == Qt::ControlModifier
			scaleView( 2**(-event.delta() / 240.0) )
		else
			super
		end
	end
	
end

class MainWindow < Qt::MainWindow
	def initialize(inspector = nil)
		super()
		
		board = TreeCanvas.new
		setCentralWidget(board)
	end
end

def self.search(space, n = 0, inspector = nil)
	app = Qt::Application.new(ARGV)
	
	mw = TreeCanvas.new(space, inspector)
	
	mw.search(n)
	mw.show
	
	app.exec
end

end

end

