# Copyright (c) 2007, David Cuadrado <krawek@gmail.com>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
# 3. Documentation and/or other materials provided with the distribution. 
#    All advertising materials mentioning features or use of this software  
#    must display the following acknowledgement: 
#    This product includes software developed by David Cuadrado and contributors.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


$: << File.dirname(__FILE__)

require 'Gecode.so'

module Gecode

module Search
	class Statistics
		attr_accessor :time
	end
end

class IntVar
	def to_s
		if min != max
			"[#{min}..#{max}]"
		else
			min.to_s
		end
	end
end

class IntVarArray
	def to_s
		res = ""
		size.times { |i|
			res += " #{at(i).to_s}"
		}
		
		res
	end
end

module MiniModel
class Matrix
	attr_reader :array, :width, :height
	def initialize(array, w, h)
		@array = array
		@width = w
		@height = h
	end
	
	def [](row, col)
		@array[@width*row+col]
	end
	
	def row(i)
		slice(0, @width, i, i+1)
	end
	
	def col(j)
		slice(j, j+1, 0, @height )
	end
	
	
	def slice(from_col, to_col, from_row, to_row)
		if( to_col > @width or to_row > @height )
			raise "Slice out of range"
		end
		
		if( from_col >= to_col or from_row >= to_row )
			raise "Slice out of range"
		end
		
		arr = Array.new((to_col - from_col) * ( to_row - from_row ) )
		i = 0
		
		for h in (from_row..to_row)
			for w in ( from_col..to_col)
				arr[i] = self[w, h]
				i+=1
			end
		end
		
		arr
	end
	
end
end


class Space
	def method_missing(sym, *args, &block)
		key = sym.to_s
		if not intVarArray(key).nil?
			return intVarArray(key)
		elsif not setVarArray(key).nil?
			return setVarArray(key)
		elsif not boolVarArray(key).nil?
			return boolVarArray(key)
		end
		
		super
	end
end

class FD
	attr_reader :space
	
	def initialize( vars, range)
		@relations = {
			"==" => Gecode::IRT_EQ,
			"!=" => Gecode::IRT_NQ,
			"<=" => Gecode::IRT_LQ,
			"<" => Gecode::IRT_LE,
			">=" => Gecode::IRT_GQ,
			">" => Gecode::IRT_GR
		}
		
		if vars.kind_of?(Array)
			@vars = vars.map { |v| v.to_s.upcase }
		else
			@vars = Array.new(vars)
			
			vars.times { |i|
				@vars[i] = "A#{i}"
			}
		end
		
		@space = Gecode::Space.new
		
		solution = Gecode::IntVarArray.new(@space, @vars.size, range.min, range.max)
		@space.own(solution, "solution")
	end
	
	def distribute(vars = Gecode::BVAR_SIZE_MIN, vals = Gecode::BVAL_MIN)
		Gecode::branch(@space, @space.solution, vars, vals)
	end
	
	def search(n = 0)
		stop = Gecode::Search::Stop.new
		dfs = Gecode::DFS.new(@space, 8, 2, stop)
		
		counter = 0
		
		if n <= 0
			counter = 1
		end
		
		time = 0
		while not @space.nil?
			break if @space.nil? or counter == n
			
			partial = Time.now.to_f
			@space = dfs.next
			partial = Time.now.to_f - partial
			
			if not @space.nil?
				puts @space.solution
			end
			
			time += partial
			
			counter += 1
		end
		
		stats = dfs.statistics
		stats.time = time
		
		stats
	end
	
	def method_missing(sym, *args, &block)
		index = @vars.index(sym.to_s.upcase)
		
		if not index.nil?
			return @space.solution.at(index)
		else
			arr = @space.intVarArray(sym.to_s)
			
			return arr if arr
			
			arr = @space.setVarArray(sym.to_s)
			
			return arr if arr
		end
		
		super
	end
	
	def distinct(c = nil)
		if c.nil?
			Gecode::distinct(@space, @space.solution, Gecode::ICL_DEF)
		else
			Gecode::distinct(@space, c, @space.solution, Gecode::ICL_DEF)
		end
	end
	
	def post(rel)
		Gecode::post(@space, rel, Gecode::ICL_DEF)
	end
	
	def rel(var, rel, val)
		relation = @relations[rel]
		
		if relation.nil?
			relation = rel
		end
		Gecode::rel(@space, var, relation, val, Gecode::ICL_DEF)
	end
	
	def size
		@vars.size
	end
	
	def [](index)
		@space.intVarArray("solution").at(index)
	end
	
end # FD

end # Gecode
